# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

require 'ara/version'

Gem::Specification.new do |spec|
  spec.name          = 'ara'
  spec.version       = Ara::VERSION
  spec.authors       = ['enRoute team']
  spec.email         = ['dev@enroute.mobi']

  spec.summary       = 'Ruby tools for Ara, a real-time server for transport'
  spec.description   = 'Provides Ruby SDK for Ara API and munin plugins'
  spec.homepage      = 'https://bitbucket.org/enroute-mobi/ara-ruby/'
  spec.license       = 'Apache-2.0'

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^spec/})
  end
  spec.require_paths = ['lib']
  spec.required_ruby_version = '>= 2.6.0'

  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'webmock'

  spec.add_dependency 'faraday'
  spec.add_dependency 'json'
end
