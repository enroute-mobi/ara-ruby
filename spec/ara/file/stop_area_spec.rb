# frozen_string_literal: true

RSpec.describe Ara::File::StopArea do
  it 'should return the correct name' do
    expect(Ara::File::StopArea.csv_name).to eq('stop_area')
  end

  it 'should return the correct default csv values' do
    sa = Ara::File::StopArea.new(name: 'name')
    expected = ['stop_area', '', '', '', '', 'name', '{}', '[]', '{}', '{}', true, false, true]
    expect(sa.csv_attrs).to eq(expected)
  end

  it 'should raise an exception with an incorrect type' do
    sa = Ara::File::StopArea.new(codes: 'wrong_value')
    expect { sa.csv_attrs }.to raise_error(Ara::File::InvalidAttributeError)
  end

  it 'should return the correct values' do
    sa = Ara::File::StopArea.new(
      id: 'id',
      name: 'name',
      parent_id: 'parent_id',
      referent_id: 'referent_id',
      model_name: 'model_name',
      codes: { a: :b },
      line_ids: ['line_id'],
      attributes: { c: :d },
      references: { e: :f },
      collected_always: false,
      collect_children: true,
      collect_situations: false
    )
    expected = ['stop_area', 'id', 'parent_id', 'referent_id', 'model_name', 'name', '{"a":"b"}', '["line_id"]',
                '{"c":"d"}', '{"e":"f"}', false, true, false]
    expect(sa.csv_attrs).to eq(expected)
  end
end
