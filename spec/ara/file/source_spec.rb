# frozen_string_literal: true

RSpec.describe Ara::File::Source do
  describe '.stop_areas' do
    subject { source.stop_areas.to_a }

    self::CSV = %(
      stop_area,a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11,b0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11,c0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11,2017-01-01,Name,"{""codeSpace"":""stopAreaCode""}","[""d0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11"",""e0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11""]","{""AttributeKey"":""abcd""}","{""ReferenceType"":{""Code"":{""codeSpace"":""5678""},""Id"":""42""}}",true,true,true
    ).strip

    context "when the file contains '#{self::CSV}'" do
      let(:content) { self.class::CSV }
      let(:file) do
        Tempfile.new.tap do |f|
          f.puts content
          f.close
        end.path
      end
      let(:source) { Ara::File::Source.new(file) }

      it do
        stop_area = an_object_having_attributes(
          id: 'a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11',
          parent_id: 'b0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11',
          referent_id: 'c0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11',
          model_name: '2017-01-01',
          name: 'Name',
          codes: { 'codeSpace' => 'stopAreaCode' },
          line_ids: %w[
            d0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11
            e0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11
          ],
          attributes: { 'AttributeKey' => 'abcd' },
          references: { 'ReferenceType' => { 'Code' => { 'codeSpace' => '5678' }, 'Id' => '42' } },
          collected_always: true,
          collect_children: true,
          collect_situations: true
        )
        is_expected.to include(stop_area)
      end
    end
  end

  describe '.stop_visits' do
    subject { source.stop_visits.to_a }

    self::CSV = %(
      stop_visit,a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11,2017-01-01,"{""codeSpace"":""VJCode""}",a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a12,a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a13,1,"[{""Kind"":""expected"",""ArrivalTime"":""2017-08-17T10:45:50+02:00"",""DepartureTime"":""2017-08-17T10:45:55+02:00""}]","{""AttributeKey"":""abcd""}","{""ReferenceType"":{""Code"":{""codeSpace"":""5678""},""Id"":""42""}}"
    ).strip

    context "when the file contains '#{self::CSV}'" do
      let(:content) { self.class::CSV }
      let(:file) do
        Tempfile.new.tap do |f|
          f.puts content
          f.close
        end.path
      end
      let(:source) { Ara::File::Source.new(file) }

      it do
        stop_visit = an_object_having_attributes(
          id: 'a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11',
          model_name: '2017-01-01',
          codes: { 'codeSpace' => 'VJCode' },
          stop_area_id: 'a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a12',
          vehicle_journey_id: 'a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a13',
          passage_order: '1',
          schedules: [{ 'Kind' => 'expected', 'ArrivalTime' => '2017-08-17T10:45:50+02:00',
                        'DepartureTime' => '2017-08-17T10:45:55+02:00' }],
          attributes: { 'AttributeKey' => 'abcd' },
          references: { 'ReferenceType' => { 'Code' => { 'codeSpace' => '5678' }, 'Id' => '42' } }
        )
        is_expected.to include(stop_visit)
      end
    end
  end

  describe '.lines' do
    subject { source.lines.to_a }

    self::CSV = %(
      line,a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11,2017-01-01,L1,"{""a"":""b""}","{""c"":""d""}","{""e"":""f""}",false,"",referent_id
    ).strip

    context "when the file contains '#{self::CSV}'" do
      let(:content) { self.class::CSV }
      let(:file) do
        Tempfile.new.tap do |f|
          f.puts content
          f.close
        end.path
      end
      let(:source) { Ara::File::Source.new(file) }

      it do
        line = an_object_having_attributes(
          id: 'a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11',
          model_name: '2017-01-01',
          name: 'L1',
          codes: { 'a' => 'b' },
          attributes: { 'c' => 'd' },
          references: { 'e' => 'f' },
          collect_situations: false,
          number: nil,
          referent_id: 'referent_id'
        )
        is_expected.to include(line)
      end
    end
  end

  describe '.stop_area_groups' do
    subject { source.stop_area_groups.to_a }

    self::CSV = %(
      stop_area_group,a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11,2017-01-01,name,short_name,"[""stop_area_id""]").strip

    context "when the file contains '#{self::CSV}'" do
      let(:content) { self.class::CSV }
      let(:file) do
        Tempfile.new.tap do |f|
          f.puts content
          f.close
        end.path
      end
      let(:source) { Ara::File::Source.new(file) }

      it do
        stop_area_group = an_object_having_attributes(
          id: 'a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11',
          model_name: '2017-01-01',
          name: 'name',
          short_name: 'short_name',
          stop_area_ids: ['stop_area_id']
        )
        is_expected.to include(stop_area_group)
      end
    end
  end

  describe '.line_groups' do
    subject { source.line_groups.to_a }

    self::CSV = %(
      line_group,a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11,2017-01-01,name,short_name,"[""line_id""]").strip

    context "when the file contains '#{self::CSV}'" do
      let(:content) { self.class::CSV }
      let(:file) do
        Tempfile.new.tap do |f|
          f.puts content
          f.close
        end.path
      end
      let(:source) { Ara::File::Source.new(file) }

      it do
        line_group = an_object_having_attributes(
          id: 'a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11',
          model_name: '2017-01-01',
          name: 'name',
          short_name: 'short_name',
          line_ids: ['line_id']
        )
        is_expected.to include(line_group)
      end
    end
  end
end
