# frozen_string_literal: true

RSpec.describe Ara::File::Line do
  it 'should return the correct name' do
    expect(Ara::File::Line.csv_name).to eq('line')
  end

  it 'should return the correct default csv values' do
    line = Ara::File::Line.new(name: 'name')
    expected = ['line', '', '', 'name', '{}', '{}', '{}', true, '', '']
    expect(line.csv_attrs).to eq(expected)
  end

  it 'should raise an exception with an incorrect type' do
    line = Ara::File::Line.new(codes: 'wrong_value')
    expect { line.csv_attrs }.to raise_error(Ara::File::InvalidAttributeError)
  end

  it 'should return the correct values' do
    line = Ara::File::Line.new(
      id: 'id',
      model_name: 'model_name',
      name: 'name',
      number: 'T4',
      codes: { a: :b },
      attributes: { c: :d },
      references: { e: :f },
      collect_situations: false,
      referent_id: 'referent_id'
    )
    expected = ['line', 'id', 'model_name', 'name', '{"a":"b"}', '{"c":"d"}', '{"e":"f"}', false, 'T4', 'referent_id']
    expect(line.csv_attrs).to eq(expected)
  end
end
