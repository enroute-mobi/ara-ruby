# frozen_string_literal: true

RSpec.describe Ara::File::Target do
  it 'should reuse model_names' do
    target = Ara::File::Target.new('output.csv')
    target.model_name(Date.today) do |model_name|
      model_name << Ara::File::StopArea.new(name: 'sa1')
    end
    target.model_name(Date.today) do |model_name|
      model_name << Ara::File::StopArea.new(name: 'sa2')
    end
    target.model_name(Date.today) do |model_name|
      model_name << Ara::File::Line.new(name: 'l1')
    end
    expect(target.instance_variable_get('@model_names').length).to eq 1
    expect(target.instance_variable_get('@model_names')[Date.today].stop_areas.length).to eq 2
    expect(target.instance_variable_get('@model_names')[Date.today].lines.length).to eq 1
  end

  it 'should handle correctly model_names' do
    target = Ara::File::Target.new('output.csv')

    (Date.today..Date.today + 1).each do |day|
      target.model_name(day) do |model_name|
        model_name << Ara::File::StopArea.new(codes: { "public": 'ORLEANS:StopArea:00004821', "ineo": '00004821' })
        model_name << Ara::File::StopArea.new(codes: { "public": 'ORLEANS:StopArea:00004821', "ineo": '00004821' })
        model_name << Ara::File::Line.new(codes: { "public": 'ORLEANS:Line:3', "ineo": '3' })
      end
    end

    expect(target.instance_variable_get('@model_names').length).to eq 2
    target.instance_variable_get('@model_names').each_value do |mn|
      expect(mn.stop_areas.length).to eq 2
      expect(mn.lines.length).to eq 1
    end
  end

  it 'should correctly export a CSV file' do
    Dir.mktmpdir('rspec-') do |dir|
      path = "#{dir}/export.csv"
      target = Ara::File::Target.new(path)

      (Date.today..Date.today + 1).each do |day|
        target.model_name(day) do |model_name|
          model_name << Ara::File::StopArea.new(
            id: 'id',
            name: 'name',
            parent_id: 'parent_id',
            referent_id: 'referent_id',
            model_name: 'model_name',
            codes: { a: 'b' },
            line_ids: ['line_id'],
            attributes: { c: :d },
            references: { e: :f },
            collected_always: false,
            collect_children: true,
            collect_situations: false
          )
          model_name << Ara::File::Line.new(
            id: 'id',
            model_name: 'model_name',
            name: 'name',
            codes: { a: :b },
            attributes: { c: :d },
            references: { e: :f },
            collect_situations: false,
            referent_id: 'referent_id'
          )
          model_name << Ara::File::StopAreaGroup.new(
            id: 'id',
            model_name: 'model_name',
            name: 'name',
            short_name: 'short_name',
            stop_area_ids: ['stop_area_id']
          )
          model_name << Ara::File::LineGroup.new(
            id: 'id',
            model_name: 'model_name',
            name: 'name',
            short_name: 'short_name',
            line_ids: ['line_id']
          )
        end
      end
      target.close

      expected_csv = <<~CSV
        stop_area,id,parent_id,referent_id,#{Date.today},name,"{""a"":""b""}","[""line_id""]","{""c"":""d""}","{""e"":""f""}",false,true,false
        stop_area_group,id,#{Date.today},name,short_name,"[""stop_area_id""]"
        line,id,#{Date.today},name,"{""a"":""b""}","{""c"":""d""}","{""e"":""f""}",false,"",referent_id
        line_group,id,#{Date.today},name,short_name,"[""line_id""]"
        stop_area,id,parent_id,referent_id,#{Date.today + 1},name,"{""a"":""b""}","[""line_id""]","{""c"":""d""}","{""e"":""f""}",false,true,false
        stop_area_group,id,#{Date.today + 1},name,short_name,"[""stop_area_id""]"
        line,id,#{Date.today + 1},name,"{""a"":""b""}","{""c"":""d""}","{""e"":""f""}",false,"",referent_id
        line_group,id,#{Date.today + 1},name,short_name,"[""line_id""]"
      CSV

      expect(File.read(path)).to eq(expected_csv)
    end
  end
end
