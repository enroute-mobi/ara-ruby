# frozen_string_literal: true

RSpec.describe Ara::File::StopAreaGroup do
  it 'should return the correct name' do
    expect(Ara::File::StopAreaGroup.csv_name).to eq('stop_area_group')
  end

  it 'should return the correct default csv values' do
    sa = Ara::File::StopAreaGroup.new(name: 'name')
    expected = ['stop_area_group', '', '', 'name', '', '[]']
    expect(sa.csv_attrs).to eq(expected)
  end

  it 'should raise an exception with an incorrect type' do
    sa = Ara::File::StopAreaGroup.new(stop_area_ids: {})
    expect { sa.csv_attrs }.to raise_error(Ara::File::InvalidAttributeError)
  end

  it 'should return the correct values' do
    sa = Ara::File::StopAreaGroup.new(
      id: 'id',
      name: 'name',
      short_name: 'short_name',
      model_name: 'model_name',
      stop_area_ids: ['stop_area_id']
    )
    expected = ['stop_area_group', 'id', 'model_name', 'name', 'short_name', '["stop_area_id"]']
    expect(sa.csv_attrs).to eq(expected)
  end
end
