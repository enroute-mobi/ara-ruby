# frozen_string_literal: true

RSpec.describe Ara::File::StopVisit do
  it 'should return the correct name' do
    expect(Ara::File::StopVisit.csv_name).to eq('stop_visit')
  end

  it 'should return the correct default csv values' do
    sv = Ara::File::StopVisit.new
    expected = ['stop_visit', '', '', '{}', '', '', '', '[]', '{}', '{}']
    expect(sv.csv_attrs).to eq(expected)
  end

  it 'should raise an exception with an incorrect type' do
    sv = Ara::File::StopVisit.new(codes: 'wrong_value')
    expect { sv.csv_attrs }.to raise_error(Ara::File::InvalidAttributeError)
  end

  it 'should return the correct values' do
    sv = Ara::File::StopVisit.new(
      id: 'id',
      model_name: 'model_name',
      codes: { 'a' => 'b' },
      stop_area_id: 'stop_area_id',
      vehicle_journey_id: 'vehicle_journey_id',
      passage_order: '1',
      schedules: [{ 'a' => 'b' }, { 'c' => 'd' }],
      attributes: { 'a' => 'b' },
      references: { 'a' => 'b' }
    )
    expected = ['stop_visit', 'id', 'model_name', '{"a":"b"}', 'stop_area_id', 'vehicle_journey_id', '1',
                '[{"a":"b"},{"c":"d"}]', '{"a":"b"}', '{"a":"b"}']
    expect(sv.csv_attrs).to eq(expected)
  end
end
