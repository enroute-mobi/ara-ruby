# frozen_string_literal: true

RSpec.describe Ara::File::Operator do
  describe '#csv_name' do
    subject { Ara::File::Operator.csv_name }
    it { is_expected.to eq('operator') }
  end

  describe '.csv_attrs' do
    let(:operator) { Ara::File::Operator.new }
    subject { operator.csv_attrs }

    context "when Operator id is 'dummy'" do
      before { operator.id = 'dummy' }
      it { is_expected.to eq(['operator', 'dummy', '', '', '{}']) }
    end

    context "when Operator model name is '2030-01-01'" do
      before { operator.model_name = '2030-01-01' }
      it { is_expected.to eq(['operator', '', '2030-01-01', '', '{}']) }
    end

    context "when Operator name is 'Operator Sample'" do
      before { operator.name = 'Operator Sample' }
      it { is_expected.to eq(['operator', '', '', 'Operator Sample', '{}']) }
    end

    context "when Operator codes is {external: 'ABC'}" do
      before { operator.codes = { external: '42' } }
      it { is_expected.to eq(['operator', '', '', '', '{"external":"42"}']) }
    end
  end
end
