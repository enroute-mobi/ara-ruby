# frozen_string_literal: true

RSpec.describe Ara::File::Validator do
  subject(:validator) { Ara::File::Validator.new(source) }

  class self::MockSource
    def stop_areas
      @stop_areas ||= []
    end

    def stop_area_groups
      @stop_area_groups ||= []
    end

    def lines
      @lines ||= []
    end

    def line_groups
      @line_groups ||= []
    end

    def vehicle_journeys
      @vehicle_journeys ||= []
    end

    def stop_visits
      @stop_visits ||= []
    end
  end

  let(:source) { self.class::MockSource.new }

  describe 'validate' do
    before { source.vehicle_journeys << Ara::File::VehicleJourney.new(id: 'invalid') }

    it 'should create errors only once' do
      validator.validate

      expect { validator.validate }.to_not change(validator, :errors)
    end
  end

  describe 'valid?' do
    context 'when no Error is present' do
      it { is_expected.to be_valid }
    end
    context 'when a Error is present' do
      before { allow(validator).to receive(:errors).and_return([Ara::File::Validator::Error.new(double, 'dummy')]) }

      it { is_expected.to_not be_valid }
    end
  end

  describe 'errors' do
    subject do
      validator.validate
      validator.errors
    end

    context "when a Stop Area has a parent_id with doesn't match any Stop Area" do
      let(:resource) { Ara::File::StopArea.new(id: 'orphelin', parent_id: 'wrong') }
      before { source.stop_areas << resource }

      it {
        is_expected.to contain_exactly(an_object_having_attributes(message: 'Unknown parent_id', resource: resource))
      }
    end

    context "when a Stop Area has a referent_id with doesn't match any Stop Area" do
      let(:resource) { Ara::File::StopArea.new(id: 'orphelin', referent_id: 'wrong') }
      before { source.stop_areas << resource }

      it {
        is_expected.to contain_exactly(an_object_having_attributes(message: 'Unknown referent_id', resource: resource))
      }
    end

    context "when a StopArea has a line_id in line_ids which doesn't match any Line" do
      let(:resource) { Ara::File::StopArea.new(id: 'id', line_ids: ['wrong']) }
      before { source.stop_areas << resource }

      it {
        is_expected.to contain_exactly(an_object_having_attributes(message: 'Unknown line_id wrong',
                                                                   resource: resource))
      }
    end

    context 'when a StopArea has a valid line_id in line_ids' do
      let(:resource) { Ara::File::StopArea.new(id: 'id', line_ids: ['valid']) }
      before do
        source.lines << Ara::File::Line.new(id: 'valid')
        source.stop_areas << resource
      end
    end

    context "When a Line has a referent_id which doesn't match any Line" do
      let(:resource) { Ara::File::Line.new(id: 'orphelin', referent_id: 'wrong') }
      before { source.lines << resource }

      it {
        is_expected.to contain_exactly(an_object_having_attributes(message: 'Unknown referent_id', resource: resource))
      }
    end

    context 'When a Line has a valid referent_id' do
      let(:resource) { Ara::File::Line.new(id: 'orphelin', referent_id: 'valid') }
      before do
        source.lines << Ara::File::Line.new(id: 'valid')
        source.lines << resource
      end

      it { is_expected.to be_empty }
    end

    context "When a Line Group has a line_id which doesn't match any Line" do
      let(:resource) { Ara::File::LineGroup.new(id: 'orphelin', line_ids: ['wrong']) }
      before { source.line_groups << resource }

      it {
        is_expected.to contain_exactly(an_object_having_attributes(message: 'Unknown line_id wrong', 
resource: resource))
      }
    end

    context "When a Stop Area Group has a stop_area_id which doesn't match any Stop Area" do
      let(:resource) { Ara::File::StopAreaGroup.new(id: 'orphelin', stop_area_ids: ['wrong']) }
      before { source.stop_area_groups << resource }

      it {
        is_expected.to contain_exactly(an_object_having_attributes(message: 'Unknown stop_area_id wrong', 
resource: resource))
      }
    end

    context "when a Vehicle Journey has a line_id with doesn't match any Line" do
      let(:resource) { Ara::File::VehicleJourney.new(id: 'orphelin', line_id: 'wrong') }
      before { source.vehicle_journeys << resource }

      it {
        is_expected.to contain_exactly(an_object_having_attributes(message: 'Unknown line_id', resource: resource))
      }
    end

    context "when a Stop Visit has a vehicle_journey_id with doesn't match any Vehicle Journey" do
      let(:resource) do
        Ara::File::StopVisit.new id: 'orphelin', stop_area_id: 'stop_area', vehicle_journey_id: 'wrong'
      end

      before do
        source.stop_areas << Ara::File::StopArea.new(id: 'stop_area')
        source.stop_visits << resource
      end

      it {
        is_expected.to contain_exactly(an_object_having_attributes(message: 'Unknown vehicle_journey_id',
                                                                   resource: resource))
      }
    end

    context 'when a Stop Visit has no stop_area_id' do
      let(:resource) do
        Ara::File::StopVisit.new id: 'orphelin', vehicle_journey_id: 'vehicle_journey'
      end

      before do
        source.lines << Ara::File::Line.new(id: 'line')
        source.vehicle_journeys << Ara::File::VehicleJourney.new(id: 'vehicle_journey', line_id: 'line')
        source.stop_visits << resource
      end

      it {
        is_expected.to contain_exactly(an_object_having_attributes(message: 'No stop_area_id',
                                                                   resource: resource))
      }
    end

    context "when a Stop Visit has a stop_area_id with doesn't match any Stop Area" do
      let(:resource) do
        Ara::File::StopVisit.new id: 'orphelin', stop_area_id: 'wrong', vehicle_journey_id: 'vehicle_journey'
      end

      before do
        source.lines << Ara::File::Line.new(id: 'line')
        source.vehicle_journeys << Ara::File::VehicleJourney.new(id: 'vehicle_journey', line_id: 'line')
        source.stop_visits << resource
      end

      it {
        is_expected.to contain_exactly(an_object_having_attributes(message: 'Unknown stop_area_id',
                                                                   resource: resource))
      }
    end

    context 'when a StopVisit has a valid vehicle_journey_id' do
      let(:resource) do
        Ara::File::StopVisit.new id: 'valid', stop_area_id: 'stop_area', vehicle_journey_id: 'vehicle_journey'
      end

      before do
        source.stop_areas << Ara::File::StopArea.new(id: 'stop_area')
        source.lines << Ara::File::Line.new(id: 'line')
        source.vehicle_journeys << Ara::File::VehicleJourney.new(id: 'vehicle_journey', line_id: 'line')

        source.stop_visits << resource
      end

      it { is_expected.to be_empty }
    end
  end
end
