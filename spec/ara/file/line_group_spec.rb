# Frozen_string_literal: true

RSpec.describe Ara::File::LineGroup do
  it 'should return the correct name' do
    expect(Ara::File::LineGroup.csv_name).to eq('line_group')
  end

  it 'should return the correct default csv values' do
    sa = Ara::File::LineGroup.new(name: 'name')
    expected = ['line_group', '', '', 'name', '', '[]']
    expect(sa.csv_attrs).to eq(expected)
  end

  it 'should raise an exception with an incorrect type' do
    sa = Ara::File::LineGroup.new(line_ids: {})
    expect { sa.csv_attrs }.to raise_error(Ara::File::InvalidAttributeError)
  end

  it 'should return the correct values' do
    sa = Ara::File::LineGroup.new(
      id: 'id',
      name: 'name',
      short_name: 'short_name',
      model_name: 'model_name',
      line_ids: ['line_id']
    )
    expected = ['line_group', 'id', 'model_name', 'name', 'short_name', '["line_id"]']
    expect(sa.csv_attrs).to eq(expected)
  end
end
