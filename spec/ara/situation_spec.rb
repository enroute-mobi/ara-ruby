# Frozen_string_literal: true

RSpec.describe Ara::Situations do
  let(:token) { 'secret' }
  let(:server) { Ara::Server.new(host: 'http://ara-api', token: token) }
  subject(:referential) { Ara::Referential.new(slug: 'test', tokens: [token]).connected_with(server) }

  describe '#all' do
    subject { referential.situations.all }
    before do
      stub_request(:get, 'ara-api/test/situations').to_return(
        body: '[{"Id":"first"},{"Id":"second"}]',
        headers: { 'Content-Type' => 'application/json' }
      )
    end
    it 'sends a request to "/test/situations"' do
      subject
      expect(WebMock).to have_requested(:get, 'ara-api/test/situations')
    end

    it {
      is_expected.to contain_exactly(an_object_having_attributes(id: 'first'),
                                     an_object_having_attributes(id: 'second'))
    }
  end

  describe '#find' do
    subject { referential.situations.find(query) }

    before do
      stub_request(:get, 'ara-api/test/situations/1').to_return(body: '{"Id":"first"}',
                                                                headers: { 'Content-Type' => 'application/json' })
    end

    let(:query) { '1' }
    it 'sends a request to "test/situations/1"' do
      subject
      expect(WebMock).to have_requested(:get, 'ara-api/test/situations/1')
    end

    it { is_expected.to have_attributes(id: 'first') }

    context 'when the server returns an error' do
      before { stub_request(:get, 'ara-api/test/situations/1').to_raise(Faraday::ResourceNotFound) }
      it { is_expected.to be nil }
    end
  end
end

RSpec.describe Ara::Situation do
  subject(:situation) { Ara::Situation.new }

  context 'when creating a situation' do
    subject(:situation) { Ara::Situation.new }
    let(:collection_connection) { double }

    before do
      allow(situation).to receive(:collection_connection).and_return(collection_connection)
    end

    let(:response) { double(body: {}) }

    it 'sends a POST request' do
      expect(collection_connection).to receive(:post) { response }

      subject.save
    end

    it 'sends the model_attributes as JSON payload' do
      subject.version = '1'

      request = Struct.new(:body).new
      allow(collection_connection).to receive(:post) do |&block|
        block.call(request)
        response
      end

      subject.save

      expect(request.body).to eq(version: '1', affects: [], info_links: [], validity_periods: [])
    end

    it 'assigns the new values to the situation model_attributes' do
      allow(collection_connection).to receive(:post) { double(body: { version: 1 }) }

      subject.save

      expect(subject.version).to eq(1)
    end

    it 'manages error returned by the API' do
      body = { errors: { summary: ['Can\'t be empty'] } }
      allow(collection_connection).to receive(:post).and_raise(Faraday::UnprocessableEntityError.new(body: body))

      subject.save

      expect(subject.errors).not_to be_empty
    end
  end

  context 'when updating a situation' do
    subject(:situation) { Ara::Situation.new(id: 1) }
    let(:resource_connection) { double }

    before do
      allow(situation).to receive(:resource_connection).and_return(resource_connection)
    end

    let(:response) { double(body: {}) }

    it 'sends a PUT request' do
      expect(resource_connection).to receive(:put) { response }

      subject.save
    end

    it 'sends the model_attributes as JSON payload' do
      subject.version = '1'

      request = Struct.new(:body).new
      allow(resource_connection).to receive(:put) do |&block|
        block.call(request)
        response
      end

      subject.save

      expect(request.body).to eq({ id: 1, version: '1', info_links: [], affects: [], validity_periods: [] })
    end

    it 'assigns the new values to the referetial model_attributes' do
      allow(resource_connection).to receive(:put) { double(body: { version: '1' }) }

      subject.save

      expect(subject.version).to eq('1')
    end

    it 'manages error returned by the API' do
      body = { errors: { summary: ['Can\'t be empty'] } }
      allow(resource_connection).to receive(:put).and_raise(Faraday::UnprocessableEntityError.new(body: body))

      subject.save

      expect(subject.errors).not_to be_empty
    end
  end

  context 'when deleting a situation' do
    subject(:situation) { Ara::Situation.new(id: 1) }
    let(:resource_connection) { double }

    let(:response) { double(body: {}) }

    before do
      allow(situation).to receive(:resource_connection).and_return(resource_connection)
    end

    it 'sends a DELETE request' do
      expect(resource_connection).to receive(:delete) { response }

      subject.destroy
    end

    it 'assigns the new values to the referetial model_attributes' do
      expect(resource_connection).to receive(:delete) { double(body: { id: '1' }) }

      subject.destroy

      expect(subject.id).to eq('1')
    end

    it 'manages error returned by the API' do
      expect(resource_connection).to receive(:delete).and_raise(Faraday::ClientError)

      expect(subject.destroy).to be_falsey
    end
  end

  describe 'creating from attributes' do
    subject(:situation) { Ara::Situation.new(attributes) }

    describe 'with a summary' do
      subject { situation.summary }
      context 'when attributes is {"summary": { "default_value: "lorem ipsum"} }' do
        let(:attributes) { { 'summary' => { 'default_value' => 'lorem ipsum' } } }
        it { is_expected.to be_a Ara::Situation::TranslatedString }
        it { is_expected.to have_attributes(default_value: 'lorem ipsum') }
      end
    end

    describe 'with a description' do
      subject { situation.description }
      context 'when attributes is {"description": { "default_value: "lorem ipsum"} }' do
        let(:attributes) { { 'description' => { 'default_value' => 'lorem ipsum' } } }
        it { is_expected.to be_a Ara::Situation::TranslatedString }
        it { is_expected.to have_attributes(default_value: 'lorem ipsum') }
      end
    end

    describe 'with validity periods' do
      subject { situation.validity_periods }
      context 'when attributes is {"validity_periods": \
[{"StartTime":"2020-01-01 12:00:00 +0200", "EndTime": "2030-01-01 21:00:00 +0200"}]}' do
        let(:attributes) do
          { 'validity_periods' => [{ 'StartTime' => '2020-01-01 12:00:00 +0200',
                                     'EndTime' => '2030-01-01 21:00:00 +0200' }] }
        end
        it {
          is_expected.to match_array(
            [
              have_attributes(
                class: Ara::Situation::TimeRange,
                start_time: Time.parse('2020-01-01 12:00:00 +0200'),
                end_time: Time.parse('2030-01-01 21:00:00 +0200')
              )
            ]
          )
        }
      end
    end

    context 'when attributes is {"id"=>"6ba7b814-9dad-11d1-0002-00c04fd430c8"}' do
      let(:attributes) { { id: '6ba7b814-9dad-11d1-0002-00c04fd430c8' } }
      it { is_expected.to have_attributes(id: '6ba7b814-9dad-11d1-0002-00c04fd430c8') }
    end

    context 'when attributes is {codes: {"a": "b"}}' do
      let(:attributes) { { codes: { 'a': 'b' } } }
      it { is_expected.to have_attributes(codes: { 'a': 'b' }) }
    end

    context 'when attributes is {affects: [{type: "Line", line_id: "9109a385-289d-42f8-8277-f7e7c632ff00" }]}' do
      let(:attributes) { { affects: [{ 'Type': 'Line', 'LineId': '9109a385-289d-42f8-8277-f7e7c632ff00' }] } }

      it {
        is_expected.to have_attributes(affects: [{ LineId: '9109a385-289d-42f8-8277-f7e7c632ff00', Type: 'Line' }])
      }
    end
  end
end
