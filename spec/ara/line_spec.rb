# Frozen_string_literal: true

RSpec.describe Ara::Lines do
  let(:token) { 'secret' }
  let(:server) { Ara::Server.new(host: 'http://ara-api', token: token) }
  subject(:referential) { Ara::Referential.new(slug: 'test', tokens: [token]).connected_with(server) }

  describe '#all' do
    subject { referential.lines.all }
    before do
      stub_request(:get, 'ara-api/test/lines').to_return(
        body: '[{"Id":"first"},{"Id":"second"}]',
        headers: { 'Content-Type' => 'application/json' }
      )
    end
    it 'sends a request to "/test/lines"' do
      subject
      expect(WebMock).to have_requested(:get, 'ara-api/test/lines')
    end

    it {
      is_expected.to contain_exactly(an_object_having_attributes(id: 'first'),
                                     an_object_having_attributes(id: 'second'))
    }
  end

  describe '#find' do
    subject { referential.lines.find(query) }

    before do
      stub_request(:get, 'ara-api/test/lines/1').to_return(body: '{"Id":"first"}',
                                                           headers: { 'Content-Type' => 'application/json' })
    end

    let(:query) { '1' }
    it 'sends a request to "test/lines/1"' do
      subject
      expect(WebMock).to have_requested(:get, 'ara-api/test/lines/1')
    end

    it { is_expected.to have_attributes(id: 'first') }

    context 'when the server returns an error' do
      before { stub_request(:get, 'ara-api/test/lines/1').to_raise(Faraday::ResourceNotFound) }
      it { is_expected.to be nil }
    end
  end
end

RSpec.describe Ara::Line do
  subject(:line) { Ara::Line.new }

  context 'when creating a line' do
    subject(:line) { Ara::Line.new }
    let(:collection_connection) { double }

    before do
      allow(line).to receive(:collection_connection).and_return(collection_connection)
    end

    let(:response) { double(body: {}) }

    it 'sends a POST request' do
      expect(collection_connection).to receive(:post) { response }

      subject.save
    end

    it 'sends the model_attributes as JSON payload' do
      subject.name = 'MaProvince'

      request = Struct.new(:body).new
      allow(collection_connection).to receive(:post) do |&block|
        block.call(request)
        response
      end

      subject.save

      expect(request.body).to eq(name: 'MaProvince')
    end

    it 'assigns the new values to the line model_attributes' do
      allow(collection_connection).to receive(:post) { double(body: { name: 'MaProvince' }) }

      subject.save

      expect(subject.name).to eq('MaProvince')
    end

    it 'manages error returned by the API' do
      # TODO
    end
  end

  context 'when updating a line' do
    subject(:line) { Ara::Line.new(id: 1) }
    let(:resource_connection) { double }

    before do
      allow(line).to receive(:resource_connection).and_return(resource_connection)
    end

    let(:response) { double(body: {}) }

    it 'sends a PUT request' do
      expect(resource_connection).to receive(:put) { response }

      subject.save
    end

    it 'sends the model_attributes as JSON payload' do
      subject.name = 'MaLigne'

      request = Struct.new(:body).new
      allow(resource_connection).to receive(:put) do |&block|
        block.call(request)
        response
      end

      subject.save

      expect(request.body).to eq({ id: 1, name: 'MaLigne' })
    end

    it 'assigns the new values to the referetial model_attributes' do
      allow(resource_connection).to receive(:put) { double(body: { name: 'MaLigne' }) }

      subject.save

      expect(subject.name).to eq('MaLigne')
    end

    it 'manages error returned by the API' do
      # TODO
    end
  end

  context 'when deleting a line' do
    subject(:line) { Ara::Line.new(id: 1) }
    let(:resource_connection) { double }

    let(:response) { double(body: {}) }

    before do
      allow(line).to receive(:resource_connection).and_return(resource_connection)
    end

    it 'sends a DELETE request' do
      expect(resource_connection).to receive(:delete) { response }

      subject.destroy
    end

    it 'assigns the new values to the referetial model_attributes' do
      expect(resource_connection).to receive(:delete) { double(body: { id: '1' }) }

      subject.destroy

      expect(subject.id).to eq('1')
    end

    it 'manages error returned by the API' do
      # TODO
    end
  end
end
