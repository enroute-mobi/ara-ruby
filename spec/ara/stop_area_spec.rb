# Frozen_string_literal: true

RSpec.describe Ara::StopAreas do
  let(:token) { 'secret' }
  let(:server) { Ara::Server.new(host: 'http://ara-api', token: token) }
  subject(:referential) { Ara::Referential.new(slug: 'test', tokens: [token]).connected_with(server) }

  describe '#all' do
    subject { referential.stop_areas.all }
    before do
      stub_request(:get, 'ara-api/test/stop_areas').to_return(
        body: '[{"Id":"first"},{"Id":"second"}]',
        headers: { 'Content-Type' => 'application/json' }
      )
    end
    it 'sends a request to "/test/stop_areas"' do
      subject
      expect(WebMock).to have_requested(:get, 'ara-api/test/stop_areas')
    end

    it {
      is_expected.to contain_exactly(an_object_having_attributes(id: 'first'),
                                     an_object_having_attributes(id: 'second'))
    }
  end

  describe '#find' do
    subject { referential.stop_areas.find(query) }

    before do
      stub_request(:get, 'ara-api/test/stop_areas/1').to_return(body: '{"Id":"first"}',
                                                                headers: { 'Content-Type' => 'application/json' })
    end

    let(:query) { '1' }
    it 'sends a request to "test/stop_areas/1"' do
      subject
      expect(WebMock).to have_requested(:get, 'ara-api/test/stop_areas/1')
    end

    it { is_expected.to have_attributes(id: 'first') }

    context 'when the server returns an error' do
      before { stub_request(:get, 'ara-api/test/stop_areas/1').to_raise(Faraday::ResourceNotFound) }
      it { is_expected.to be nil }
    end
  end
end

RSpec.describe Ara::StopArea do
  subject(:stop_area) { Ara::StopArea.new }

  context 'when creating a stop_area' do
    subject(:stop_area) { Ara::StopArea.new }
    let(:collection_connection) { double }

    before do
      allow(stop_area).to receive(:collection_connection).and_return(collection_connection)
    end

    let(:response) { double(body: {}) }

    it 'sends a POST request' do
      expect(collection_connection).to receive(:post) { response }

      subject.save
    end

    it 'sends the model_attributes as JSON payload' do
      subject.name = 'MaProvince'
      subject.line_ids = ['37bb2b0b-37f8-4c19-b163-dacac443663e']

      request = Struct.new(:body).new
      allow(collection_connection).to receive(:post) do |&block|
        block.call(request)
        response
      end

      subject.save

      expect(request.body).to eq({ name: 'MaProvince', line_ids: ['37bb2b0b-37f8-4c19-b163-dacac443663e'] })
    end

    it 'assigns the new values to the stop_area model_attributes' do
      allow(collection_connection).to receive(:post) { double(body: { name: 'MaProvince' }) }

      subject.save

      expect(subject.name).to eq('MaProvince')
    end

    it 'manages error returned by the API' do
      # TODO
    end
  end

  context 'when updating a stop_area' do
    subject(:stop_area) { Ara::StopArea.new(id: 1) }
    let(:resource_connection) { double }

    before do
      allow(stop_area).to receive(:resource_connection).and_return(resource_connection)
    end

    let(:response) { double(body: {}) }

    it 'sends a PUT request' do
      expect(resource_connection).to receive(:put) { response }

      subject.save
    end

    it 'sends the model_attributes as JSON payload' do
      subject.name = 'MaLigne'

      request = Struct.new(:body).new
      allow(resource_connection).to receive(:put) do |&block|
        block.call(request)
        response
      end

      subject.save

      expect(request.body).to eq({ id: 1, name: 'MaLigne' })
    end

    it 'assigns the new values to the referetial model_attributes' do
      allow(resource_connection).to receive(:put) { double(body: { name: 'MaLigne' }) }

      subject.save

      expect(subject.name).to eq('MaLigne')
    end

    it 'manages error returned by the API' do
      # TODO
    end
  end

  context 'when deleting a stop_area' do
    subject(:stop_area) { Ara::StopArea.new(id: 1) }
    let(:resource_connection) { double }

    let(:response) { double(body: {}) }

    before do
      allow(stop_area).to receive(:resource_connection).and_return(resource_connection)
    end

    it 'sends a DELETE request' do
      expect(resource_connection).to receive(:delete) { response }

      subject.destroy
    end

    it 'assigns the new values to the referetial model_attributes' do
      expect(resource_connection).to receive(:delete) { double(body: { id: '1' }) }

      subject.destroy

      expect(subject.id).to eq('1')
    end

    it 'manages error returned by the API' do
      # TODO
    end
  end

  describe 'creating from attributes' do
    subject { Ara::StopArea.new(attributes) }

    before do
      allow(subject).to receive_message_chain(:connection_provider, :connection_provider, :lines, :find)
        .and_return(Ara::Line.new(id: '9109a385-289d-42f8-8277-f7e7c632ff00'))
    end

    context 'when attributes is {"id"=>"6ba7b814-9dad-11d1-0002-00c04fd430c8"}' do
      let(:attributes) { { id: '6ba7b814-9dad-11d1-0002-00c04fd430c8' } }
      it { is_expected.to have_attributes(id: '6ba7b814-9dad-11d1-0002-00c04fd430c8') }
    end

    context 'when attributes is {next_collect_at:"2023-01-26T04:00:00+01:00"}' do
      let(:attributes) { { next_collect_at: '2023-01-26T04:00:00+01:00' } }
      it { is_expected.to have_attributes(next_collect_at: '2023-01-26T04:00:00+01:00') }
    end

    context 'when attributes is {codes: {"a": "b"}}' do
      let(:attributes) { { codes: { 'a': 'b' } } }
      it { is_expected.to have_attributes(codes: { 'a': 'b' }) }
    end

    context 'when attributes is {lines: ["9109a385-289d-42f8-8277-f7e7c632ff00"]}' do
      let(:attributes) { { lines: ['9109a385-289d-42f8-8277-f7e7c632ff00'] } }

      it { is_expected.to have_attributes(line_ids: ['9109a385-289d-42f8-8277-f7e7c632ff00']) }

      describe '#lines' do
        it 'creates an array of Ara::Lines objects' do
          expect(subject.lines).to match_array([have_attributes(
            class: Ara::Line,
            id: '9109a385-289d-42f8-8277-f7e7c632ff00'
          )])
        end
      end
    end
  end
end
