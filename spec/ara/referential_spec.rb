# frozen_string_literal: true

RSpec.describe Ara::Referentials do
  let(:token) { 'secret' }
  let(:server) { Ara::Server.new(host: 'http://ara-api', token: token) }
  subject(:referentials) { Ara::Referentials.new.connected_with(server) }

  describe '#all' do
    subject { referentials.all }

    before { stub_request(:get, 'ara-api/_referentials').to_return(body: []) }

    it 'sends a request to "/_referentials"' do
      subject
      expect(WebMock).to have_requested(:get, 'ara-api/_referentials')
    end

    it 'sends a request with "Authorization: Token token=secret" header' do
      subject
      headers = { 'Authorization' => 'Token token=secret' }
      expect(WebMock).to have_requested(:get, 'ara-api/_referentials').with(headers: headers)
    end

    self::JSON = '[{"Id":"first"},{"Id":"second"}]'
    context "when response contains '#{self::JSON}'" do
      let(:json) { self.class::JSON }
      before do
        stub_request(:get, 'ara-api/_referentials').to_return(body: json,
                                                              headers: { 'Content-Type' => 'application/json' })
      end

      it {
        is_expected.to contain_exactly(an_object_having_attributes(id: 'first'),
                                       an_object_having_attributes(id: 'second'))
      }
    end
  end

  describe '#find' do
    subject { referentials.find(query) }

    before do
      stub_request(:get, 'ara-api/_referentials/1').to_return(body: '{"Id":"first"}',
                                                              headers: { 'Content-Type' => 'application/json' })
    end

    let(:query) { '1' }
    it 'sends a request to "_referentials"' do
      subject
      expect(WebMock).to have_requested(:get, 'ara-api/_referentials/1')
    end

    it 'sends a request with "Authorization: Token token=secret" header' do
      subject
      headers = { 'Authorization' => 'Token token=secret' }
      expect(WebMock).to have_requested(:get, 'ara-api/_referentials/1').with(headers: headers)
    end

    it { is_expected.to have_attributes(id: 'first') }

    context 'when the server returns an error' do
      before { stub_request(:get, 'ara-api/_referentials/1').to_raise(Faraday::ResourceNotFound) }
      it { is_expected.to be nil }
    end
  end

  # POST /_referentials/save
  describe '#save' do
    subject { referentials.save }

    before do
      stub_request(:post, 'ara-api/_referentials/save').to_return(body: '')
    end

    it 'send a POST request on /_referentials/save' do
      subject

      expect(WebMock).to have_requested(:post, 'ara-api/_referentials/save')
    end
  end
end

RSpec.describe Ara::Referential do
  describe '#save' do
    describe '#connection' do
      subject(:referential_connection) { referential.connection }

      let(:referential) { Ara::Referential.new(slug: 'test', tokens: ['secret']).connected_with(connection_provider) }
      let(:connection_provider) { double(connection: connection) }
      let(:connection) { Ara::Connection.new('http://ara-api') }

      it 'adds the first referential token to the headers' do
        expect(referential_connection.options.context).to include(token: 'secret')
      end

      it 'adds the referential slug to the url' do
        expect(referential_connection.url_prefix.to_s).to eq('http://ara-api/test')
      end
    end

    context 'when creating a referential' do
      # POST sur la connection
      subject(:referential) { Ara::Referential.new }

      before { allow(referential).to receive(:collection_connection) { collection_connection } }
      let(:response) { double(body: {}) }
      let(:collection_connection) { double }

      it 'sends a POST request' do
        expect(collection_connection).to receive(:post) { response }

        subject.save
      end

      it 'sends the attributes as JSON payload' do
        referential.name = 'MaProvince'

        request = Struct.new(:body).new
        allow(collection_connection).to receive(:post) do |&block|
          block.call(request)
          response
        end

        subject.save

        expect(request.body).to eq(name: referential.name)
      end

      it 'assigns the new values to the referetial attributes' do
        allow(collection_connection).to receive(:post) { double(body: { name: 'MaProvince' }) }

        subject.save

        expect(referential.name).to eq('MaProvince')
      end

      it 'manages error returned by the API' do
        body = { errors: { slug: ['Invalid format: only lowercase alphanumeric characters and _'] } }
        allow(collection_connection).to receive(:post).and_raise(Faraday::UnprocessableEntityError.new(body: body))

        subject.save

        expect(referential.errors).not_to be_empty
      end
    end

    context 'when updating a referential' do
      subject(:referential) { Ara::Referential.new(id: '1') }

      before do
        allow(referential).to receive(:resource_connection).and_return(resource_connection)
      end
      let(:response) { double(body: {}) }
      let(:resource_connection) { double }

      it 'sends a PUT request' do
        expect(resource_connection).to receive(:put) { response }

        subject.save
      end

      it 'sends the attributes as JSON payload' do
        referential.name = 'MaProvince'

        request = Struct.new(:body).new
        allow(resource_connection).to receive(:put) do |&block|
          block.call(request)
          response
        end

        subject.save

        expect(request.body).to eq({ name: 'MaProvince' })
      end

      it 'assigns the new values to the referetial attributes' do
        allow(resource_connection).to receive(:put) { double(body: { name: 'MaProvince' }) }

        subject.save

        expect(referential.name).to eq('MaProvince')
      end

      it 'manages error returned by the API' do
        body = { errors: { slug: ['Invalid format: only lowercase alphanumeric characters and _'] } }
        allow(resource_connection).to receive(:put).and_raise(Faraday::UnprocessableEntityError.new(body: body))

        subject.save

        expect(referential.errors).not_to be_empty
      end
    end

    context 'when deleting a referential' do
      subject(:referential) { Ara::Referential.new }

      let(:response) { double(body: {}) }
      let(:resource_connection) { double }

      before do
        allow(referential).to receive(:resource_connection).and_return(resource_connection)
      end

      it 'sends a DELETE request' do
        expect(resource_connection).to receive(:delete) { response }

        subject.destroy
      end

      it 'assigns the new values to the referential attributes' do
        allow(resource_connection).to receive(:delete) { double(body: { id: '1' }) }

        subject.destroy
        expect(referential.id).to eq('1')
      end

      it 'manages error returned by the API' do
        allow(resource_connection).to receive(:delete).and_raise(Faraday::ClientError)

        expect(subject.destroy).to be_falsey
      end
    end
  end

  describe 'creating from attributes' do
    subject { Ara::Referential.new(attributes) }

    context 'when attributes is {"id"=>"6ba7b814-9dad-11d1-0002-00c04fd430c8"}' do
      let(:attributes) { { id: '6ba7b814-9dad-11d1-0002-00c04fd430c8' } }
      it { is_expected.to have_attributes(id: '6ba7b814-9dad-11d1-0002-00c04fd430c8') }
    end

    context 'when attributes is {next_reload_at:"2023-01-26T04:00:00+01:00"}' do
      let(:attributes) { { next_reload_at: '2023-01-26T04:00:00+01:00' } }
      it { is_expected.to have_attributes(next_reload_at: '2023-01-26T04:00:00+01:00') }
    end

    context 'when attributes is {settings: {"model.reload_at"=>"03:00"}}}' do
      let(:attributes) { { settings: { 'model.reload_at' => '03:00' } } }

      it { is_expected.to have_attributes(settings: { 'model.reload_at' => '03:00' }) }
    end

    context 'when attributes is {partners: ["9109a385-289d-42f8-8277-f7e7c632ff00"]}' do
      let(:attributes) { { partners: ['9109a385-289d-42f8-8277-f7e7c632ff00'] } }

      it { is_expected.to have_attributes(partner_ids: ['9109a385-289d-42f8-8277-f7e7c632ff00']) }
    end
  end

  # POST /_referentials/{{referential_slug}}/reload
  describe '#reload_models' do
    subject { referential.reload_models }

    let(:referential) { Ara::Referential.new }
    let(:resource_connection) { double }

    before do
      allow(referential).to receive(:resource_connection).and_return(resource_connection)
    end

    it 'sends a POST request' do
      expect(resource_connection).to receive(:post).with('reload')
      subject
    end
  end
end
