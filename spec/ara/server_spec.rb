# frozen_string_literal: true

RSpec.describe Ara::Server do
  let(:host) { 'ara-api' }
  let(:token) { 'token' }
  subject(:server) { Ara::Server.new host: host, token: token }

  describe '#referentials' do
    subject { server.referentials }
    it { is_expected.to be_a(Ara::Referentials) }
  end
end
