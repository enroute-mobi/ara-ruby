# frozen_string_literal: true

RSpec.describe Ara::Connection::CapitalizedHash do
  subject(:middleware) { Ara::Connection::CapitalizedHash.new }

  describe '#underscore' do
    subject { middleware.underscore(input) }

    [
      ['Name', :name],
      ['OrganisationId', :organisation_id]
    ].each do |key, expected|
      context "when '#{key}' is given" do
        let(:input) { key }
        it { is_expected.to eq(expected) }
      end
    end
  end
end
