# frozen_string_literal: true

RSpec.describe Ara::Partners do
  let(:token) { 'secret' }
  let(:server) { Ara::Server.new(host: 'http://ara-api', token: token) }
  subject(:referential) { Ara::Referential.new(slug: 'test', tokens: [token]).connected_with(server) }

  describe '#all' do
    subject { referential.partners.all }
    before do
      stub_request(:get, 'ara-api/test/partners').to_return(
        body: '[{"Id":"first"},{"Id":"second"}]',
        headers: { 'Content-Type' => 'application/json' }
      )
    end
    it 'sends a request to "/test/partners"' do
      subject
      expect(WebMock).to have_requested(:get, 'ara-api/test/partners')
    end

    it {
      is_expected.to contain_exactly(an_object_having_attributes(id: 'first'),
                                     an_object_having_attributes(id: 'second'))
    }
  end

  describe '#find' do
    subject { referential.partners.find(query) }

    before do
      stub_request(:get, 'ara-api/test/partners/1').to_return(body: '{"Id":"first"}',
                                                              headers: { 'Content-Type' => 'application/json' })
    end

    let(:query) { '1' }
    it 'sends a request to "test/partners/1"' do
      subject
      expect(WebMock).to have_requested(:get, 'ara-api/test/partners/1')
    end

    it { is_expected.to have_attributes(id: 'first') }

    context 'when the server returns an error' do
      before { stub_request(:get, 'ara-api/test/partners/1').to_raise(Faraday::ResourceNotFound) }
      it { is_expected.to be nil }
    end
  end

  # POST /partners/save
  describe '#save' do
    subject { referential.partners.save }

    before do
      stub_request(:post, 'ara-api/test/partners/save').to_return(body: '')
    end

    it 'send a POST request on /partners/save' do
      subject

      expect(WebMock).to have_requested(:post, 'ara-api/test/partners/save')
    end
  end
end

RSpec.describe Ara::Partner do
  subject(:partner) { Ara::Partner.new }

  describe '#status' do
    subject { partner.status }

    context 'partner_status is nil' do
      before { partner.partner_status = nil }

      it { is_expected.to be_nil }
    end

    context "partner_status['OperationnalStatus'] isn't defined" do
      before { partner.partner_status = {} }

      it { is_expected.to be_nil }
    end

    context "partner_status['OperationnalStatus'] is 'dummy'" do
      before { partner.partner_status = { 'OperationnalStatus' => 'dummy' } }

      it { is_expected.to eq 'dummy' }
    end
  end

  context 'when creating a partner' do
    subject(:partner) { Ara::Partner.new }
    let(:collection_connection) { double }

    before do
      allow(partner).to receive(:collection_connection).and_return(collection_connection)
    end

    let(:response) { double(body: {}) }

    it 'sends a POST request' do
      expect(collection_connection).to receive(:post) { response }

      subject.save
    end

    it 'sends the attributes as JSON payload' do
      subject.name = 'MaProvince'

      request = Struct.new(:body).new
      allow(collection_connection).to receive(:post) do |&block|
        block.call(request)
        response
      end

      subject.save

      expect(request.body).to eq(name: 'MaProvince')
    end

    it 'assigns the new values to the referetial attributes' do
      allow(collection_connection).to receive(:post) { double(body: { name: 'MaProvince' }) }

      subject.save

      expect(subject.name).to eq('MaProvince')
    end

    it 'manages error returned by the API' do
      body = { errors: { slug: ['Invalid format: only lowercase alphanumeric characters and _'] } }
      allow(collection_connection).to receive(:post).and_raise(Faraday::UnprocessableEntityError.new(body: body))

      subject.save

      expect(subject.errors).not_to be_empty
    end
  end

  context 'when updating a partner' do
    subject(:partner) { Ara::Partner.new(id: 1) }
    let(:resource_connection) { double }

    before do
      allow(partner).to receive(:resource_connection).and_return(resource_connection)
    end

    let(:response) { double(body: {}) }

    it 'sends a PUT request' do
      expect(resource_connection).to receive(:put) { response }

      subject.save
    end

    it 'sends the attributes as JSON payload' do
      subject.name = 'MaProvince'

      request = Struct.new(:body).new
      allow(resource_connection).to receive(:put) do |&block|
        block.call(request)
        response
      end

      subject.save

      expect(request.body).to eq(name: 'MaProvince')
    end

    it 'assigns the new values to the referetial attributes' do
      allow(resource_connection).to receive(:put) { double(body: { name: 'MaProvince' }) }

      subject.save

      expect(subject.name).to eq('MaProvince')
    end

    it 'manages error returned by the API' do
      body = { errors: { slug: ['Invalid format: only lowercase alphanumeric characters and _'] } }
      allow(resource_connection).to receive(:put).and_raise(Faraday::UnprocessableEntityError.new(body: body))

      subject.save

      expect(subject.errors).not_to be_empty
    end
  end

  context 'when deleting a partner' do
    subject(:partner) { Ara::Partner.new(id: 1) }
    let(:resource_connection) { double }

    let(:response) { double(body: {}) }

    before do
      allow(partner).to receive(:resource_connection).and_return(resource_connection)
    end

    it 'sends a DELETE request' do
      expect(resource_connection).to receive(:delete) { response }

      subject.destroy
    end

    it 'assigns the new values to the referetial attributes' do
      expect(resource_connection).to receive(:delete) { double(body: { id: '1' }) }

      subject.destroy

      expect(subject.id).to eq('1')
    end

    it 'manages error returned by the API' do
      expect(resource_connection).to receive(:delete).and_raise(Faraday::ClientError)

      expect(subject.destroy).to be_falsey
    end
  end
end
