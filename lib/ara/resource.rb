# frozen_string_literal: true

module Ara
  # Manage Ara API for a dedicated Resource (Referential, Partner, StopArea, etc)
  class Resource
    def initialize(attr = {})
      self.model_attributes = attr
    end

    def model_attributes=(attr)
      attr.each do |k, v|
        send "#{k}=", v
      end
    end

    # Manage errors associated to the Resource
    module Errors
      def errors
        @errors ||= []
      end

      def valid?
        errors.empty?
      end
    end

    include Errors

    # Manage resource identifier
    module Identifier
      def self.included(base)
        base.attr_accessor :id
      end

      def persisted?
        !id.nil?
      end
    end

    include Identifier

    # Manage resource and collection connections
    module Connection
      def collection_connection
        connection_provider.connection
      end

      def resource_connection
        collection_connection.tap do |connection|
          connection.rebase id
        end
      end

      def connection
        resource_connection
      end
    end

    include ConnectionSupport
    include Connection

    # Save resource by using post/put API endpoints
    module Save
      def save
        response = save_request do |request|
          request.body = api_attributes
        end
        self.model_attributes = response.body
        errors.clear
        true
      rescue Faraday::UnprocessableEntityError => e
        @errors = ErrorParser.new(e.response[:body]).parse

        false
      end

      private

      def save_request(&block)
        if !persisted?
          collection_connection.post do |request|
            block.call request
          end
        else
          resource_connection.put do |request|
            block.call request
          end
        end
      end
    end

    include Save

    # Destroy resource
    module Destroy
      def destroy
        response = resource_connection.delete
        self.model_attributes = response.body

        true
      rescue Faraday::ClientError
        false
      end
    end

    include Destroy
  end
end
