# frozen_string_literal: true

module Ara
  # Manage Ara Referential referentials collection
  class Referentials < Collection
    def initialize
      super collection_name: '_referentials'
    end

    # Saves all Referentials in database
    def save
      connection.post('save')
    end
  end
end
