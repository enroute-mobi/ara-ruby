# frozen_string_literal: true

module Ara
  # Manage Ara Referential Line
  class StopArea < Resource
    attr_accessor :id,
                  :attributes,
                  :codes,
                  :collect_children,
                  :collect_situations,
                  :collected_always,
                  :collected_at,
                  :collected_until,
                  :latitude,
                  :line_ids,
                  :longitude,
                  :monitored,
                  :name,
                  :next_collect_at,
                  :origins,
                  :parent_id,
                  :references,
                  :referent_id

    def api_attributes
      {
        id: id,
        name: name,
        codes: codes,
        referent_id: referent_id,
        parent_id: parent_id,
        collect_children: collect_children,
        collect_situations: collect_situations,
        collected_always: collected_always,
        monitored: monitored,
        line_ids: line_ids
      }.compact
    end

    def model_attributes=(model_attributes)
      model_attributes[:line_ids] = model_attributes.delete(:lines)
      super model_attributes
    end

    def lines
      return [] if line_ids.nil?

      @lines = line_ids.map do |line_id|
        connection_provider.connection_provider.lines.find(line_id)
      end
    end
  end
end
