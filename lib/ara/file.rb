# frozen_string_literal: true

module Ara
  module File
  end
end

require 'csv'
require 'json'
require 'delegate'

require 'ara/file/resource'
require 'ara/file/operator'
require 'ara/file/line'
require 'ara/file/line_group'
require 'ara/file/stop_area'
require 'ara/file/stop_area_group'
require 'ara/file/vehicle_journey'
require 'ara/file/stop_visit'

require 'ara/file/target'

require 'ara/file/source'
require 'ara/file/validator'
