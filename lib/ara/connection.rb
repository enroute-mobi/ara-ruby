# frozen_string_literal: true

module Ara
  # Faraday Connection setup to interact with Ara API
  class Connection < Faraday::Connection
    def initialize(url)
      super(url: url) do |connection|
        connection.use RaiseError

        # Requires connection.options.context = { token: token }
        # See https://github.com/lostisland/faraday/blob/main/docs/usage/customize.md
        connection.request :token_authorization

        connection.request :capitalized_hash
        connection.request :json

        connection.response :capitalized_hash
        connection.response :json
        # faraday.response :logger # , nil, { headers: true, bodies: true, errors: true }
      end
    end

    # Returns current request/connection context
    def context
      options.context ||= {}
    end

    # Change token to be used in request Token Authorization
    def token=(token)
      context[:token] = token
    end

    # Return token to be used in request Token Authorization
    def token
      context[:token]
    end

    # Change Connection url_prefix by appending the given base_url
    #
    # For example, with a url_prefix "/_referentials", rebase("1") changes url_prefix to "/_referentials/1"
    def rebase(base_url)
      self.url_prefix = build_url base_url
    end

    # Raises a Faraday::UnprocessableEntityError when the response status is 400
    #
    # https://github.com/lostisland/faraday/blob/main/lib/faraday/response/raise_error.rb
    class RaiseError < Faraday::Response::RaiseError
      def on_complete(env)
        raise Faraday::UnprocessableEntityError, response_values(env) if env[:status] == 400

        super
      end
    end

    # Manage Authorization header with token defined in the request/connection context
    class TokenAuthorization < Faraday::Request::Authorization
      Faraday::Request.register_middleware(token_authorization: TokenAuthorization)

      def initialize(app)
        super app, :token
      end

      private

      def header_from(_type, env, *_params)
        token = env.request.context[:token]
        raise 'Not token defined in request/connection context' unless token

        "Token token=#{token}"
      end
    end

    # Change Hash send and receive in requests to manage camelized keys
    class CapitalizedHash < Faraday::Middleware
      Faraday::Request.register_middleware(capitalized_hash: CapitalizedHash)
      Faraday::Response.register_middleware(capitalized_hash: CapitalizedHash)

      def on_request(env)
        return unless env[:body].is_a?(Hash)

        env[:body] = transform_keys(env[:body]) do |key|
          camelize key
        end
      end

      def on_complete(env)
        env[:body] = transform(env[:body]) do |key|
          underscore key
        end
      end

      def transform_keys(body)
        result = {}
        body.each_key do |key|
          result[yield(key)] = body[key]
        end
        result
      end

      def transform(value, &block)
        case value
        when Array
          value.map { |item| transform(item, &block) }
        when Hash
          transform_keys(value) do |key|
            block.call key
          end
        else
          value
        end
      end

      def underscore(camel_cased_word)
        camel_cased_word.to_s.gsub(/([A-Z]+)([A-Z][a-z])/, '\1_\2')
                        .gsub(/([a-z\d])([A-Z])/, '\1_\2')
                        .downcase.to_sym
      end

      def camelize(underscore_word)
        underscore_word.to_s.gsub(/(?:^|_)(.)/) { Regexp.last_match(1).upcase }
      end
    end
  end
end
