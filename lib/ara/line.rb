# frozen_string_literal: true

module Ara
  # Manage Ara Referential Line
  class Line < Resource
    attr_accessor :id,
                  :attributes,
                  :codes,
                  :collect_situations,
                  :collected_at,
                  :name,
                  :next_collect_at,
                  :number,
                  :origin,
                  :references,
                  :referent_id

    def api_attributes
      {
        id: id,
        name: name,
        number: number,
        codes: codes,
        origins: origin,
        collect_situations: collect_situations,
        attributes: attributes
      }.compact
    end
  end
end
