# frozen_string_literal: true

module Ara
  # Manage Ara Referential
  class Referential < Resource
    attr_accessor :slug, :next_reload_at, :name, :partner_ids, :organisation_id, :settings, :tokens, :import_tokens

    def reload_models
      # Requires the same token than _referentials
      resource_connection.post('reload')
    end

    # Provides connection to /<slug> (with referential token)
    def connection
      connection_provider.connection.tap do |connection|
        connection.rebase "/#{slug}"
        connection.token = tokens.first
      end
    end

    def model_attributes=(model_attributes)
      model_attributes[:partner_ids] = model_attributes.delete(:partners)
      super model_attributes
    end

    def api_attributes
      {
        slug: slug,
        name: name,
        tokens: tokens,
        import_tokens: import_tokens,
        settings: settings
      }.compact
    end

    def situations
      @situations ||= Ara::Situations.new.connected_with(self)
    end

    def lines
      @lines ||= Ara::Lines.new.connected_with(self)
    end

    def stop_areas
      @stop_areas ||= Ara::StopAreas.new.connected_with(self)
    end

    def partners
      @partners ||= Ara::Partners.new.connected_with(self)
    end
  end
end
