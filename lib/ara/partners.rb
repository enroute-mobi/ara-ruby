# frozen_string_literal: true

module Ara
  # Manage Ara Referential partners collection
  class Partners < Collection
    # Saves all Partners in database
    def save
      connection.post 'save'
    end
  end
end
