# frozen_string_literal: true

module Ara
  # Manage Ara API for a dedicated resource collection
  class Collection
    include Enumerable
    include ConnectionSupport

    def each(&block)
      all.each(&block)
    end

    def first
      all.first
    end

    def last
      all.last
    end

    attr_accessor :collection_name
    attr_reader :resource_class

    def initialize(options = {})
      options = self.class.default_options.merge(options)

      options.each { |k, v| send "#{k}=", v }
    end

    def self.default_options
      demodulized_name = name.split('::').last

      {
        resource_class: demodulized_name.gsub(/s$/, ''),
        collection_name: demodulized_name.downcase
      }
    end

    def resource_class=(resource_class)
      resource_class = Ara.const_get(resource_class.to_s) unless resource_class.is_a?(Class)
      @resource_class = resource_class
    end

    def all
      body = connection.get.body
      return [] unless body

      body.map do |attributes|
        build_resource attributes
      end
    end

    def find(id_or_slug)
      attributes = connection.get(id_or_slug.to_s).body
      build_resource attributes
    rescue Faraday::ResourceNotFound
      nil
    end

    def create(attributes = {})
      build_resource attributes
    end

    def connection
      connection_provider.connection.tap do |connection|
        connection.rebase collection_name
      end
    end

    protected

    def build_resource(attributes = {})
      resource_class.new(attributes).connected_with(self)
    end
  end
end
