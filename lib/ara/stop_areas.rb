# frozen_string_literal: true

module Ara
  # Manage Ara Referential stopAreas collection
  class StopAreas < Collection
    def initialize
      super collection_name: 'stop_areas'
    end
  end
end
