# frozen_string_literal: true

module Ara
  module File
    # Validate resources provided by a Ara::File::Source
    #
    # Example:
    #
    #   source = Ara::File::Source.new(file)
    #   validator = Ara::File::Validator.new(source)
    #
    #   validator.valid?
    #
    #   validator.errors.each do |error|
    #     puts error
    #   end
    #
    class Validator
      def initialize(source)
        @source = source
      end

      def valid?
        validate
        errors.empty?
      end

      attr_reader :source

      # Used to analyse each model name (and identifiers)
      class ModelName
        def initialize(name)
          @name = name
        end

        def stop_area_ids
          @stop_area_ids ||= Set.new
        end

        def stop_area_group_ids
          @stop_area_group_ids ||= Set.new
        end

        def line_ids
          @line_ids ||= Set.new
        end

        def line_group_ids
          @line_group_ids ||= Set.new
        end

        def vehicle_journey_ids
          @vehicle_journey_ids ||= Set.new
        end
      end

      def model_names
        @model_names ||= Hash.new { |h, name| h[name] = ModelName.new(name) }
      end

      def model_name(name)
        model_names[name]
      end

      def validate
        return if @validated

        @validated = true

        Loader.new(self, Ara::File::StopArea).load
        Loader.new(self, Ara::File::StopAreaGroup).load
        Loader.new(self, Ara::File::Line).load
        Loader.new(self, Ara::File::LineGroup).load
        Loader.new(self, Ara::File::VehicleJourney).load

        StopAreas.new(self).validate
        Lines.new(self).validate
        StopAreaGroups.new(self).validate
        LineGroups.new(self).validate
        StopVisits.new(self).validate
        VehicleJourneys.new(self).validate
      end

      # Load all identifiers of a given resource from source
      class Loader
        def initialize(validator, resource_class)
          @validator = validator
          @resource_class = resource_class
        end

        attr_reader :validator, :resource_class

        extend Forwardable
        def_delegators :validator, :source, :model_name
        def_delegators :resource_class, :resource_name

        def load
          resources.each do |resource|
            resource_ids(resource.model_name) << resource.id
          end
        end

        def resources
          source.send("#{resource_name}s")
        end

        def resource_ids(model_name)
          model_name(model_name).send("#{resource_name}_ids")
        end
      end

      # Base class to delegate a part of the validation process
      class Part
        def initialize(validator)
          @validator = validator
        end

        attr_reader :validator

        extend Forwardable
        def_delegators :validator, :source, :model_name, :errors

        def create_error(resource, message)
          errors << Error.new(resource, message)
        end

        # Base class used to decorate a resource to simplify validatation
        class Decorator < SimpleDelegator
          def initialize(resource, validator)
            super resource

            @resource = resource
            @validator = validator
          end

          attr_reader :resource, :validator

          def model_name
            @model_name ||= validator.model_name(resource.model_name)
          end

          def create_error(message)
            validator.errors << Error.new(resource, message)
          end
        end
      end

      # Validate all Stop Areas
      class StopAreas < Part
        def_delegators :source, :stop_areas

        def validate
          stop_areas.each do |stop_area|
            model_name = model_name(stop_area.model_name)

            if stop_area.parent_id && !model_name.stop_area_ids.include?(stop_area.parent_id)
              create_error stop_area, 'Unknown parent_id'
            end
            if stop_area.referent_id && !model_name.stop_area_ids.include?(stop_area.referent_id)
              create_error stop_area, 'Unknown referent_id'
            end
            stop_area.line_ids&.each do |line|
              create_error stop_area, "Unknown line_id #{line}" unless model_name.line_ids.include?(line)
            end
          end
        end
      end

      # Validate all Lines
      class Lines < Part
        def_delegators :source, :lines

        def validate
          lines.each do |line|
            model_name = model_name(line.model_name)

            if line.referent_id && !model_name.line_ids.include?(line.referent_id)
              create_error line, 'Unknown referent_id'
            end
          end
        end
      end

      # Validate all Stop Area Groups
      class StopAreaGroups < Part
        def_delegators :source, :stop_area_groups

        def validate
          stop_area_groups.each do |stop_area_group|
            model_name = model_name(stop_area_group.model_name)

            stop_area_group.stop_area_ids&.each do |stop_area|
              unless model_name.stop_area_ids.include?(stop_area)
                create_error stop_area_group,
                             "Unknown stop_area_id #{stop_area}"
              end
            end
          end
        end
      end

      # Validate all Stop Area Groups
      class LineGroups < Part
        def_delegators :source, :line_groups

        def validate
          line_groups.each do |line_group|
            model_name = model_name(line_group.model_name)

            line_group.line_ids&.each do |line|
              create_error line_group, "Unknown line_id #{line}" unless model_name.line_ids.include?(line)
            end
          end
        end
      end

      # Validate all Vehicle Journeys
      class VehicleJourneys < Part
        def_delegators :source, :vehicle_journeys

        def validate
          vehicle_journeys.each do |vehicle_journey|
            model_name = model_name(vehicle_journey.model_name)

            if vehicle_journey.line_id
              unless model_name.line_ids.include?(vehicle_journey.line_id)
                create_error vehicle_journey, 'Unknown line_id'
              end
            else
              create_error vehicle_journey, 'No line_id'
            end
          end
        end
      end

      # Validate all Stop Visits
      class StopVisits < Part
        def_delegators :source, :stop_visits

        def validate
          stop_visits.each do |stop_visit|
            Decorator.new(stop_visit, validator).validate
          end
        end

        # Validate a Stop Visit
        class Decorator < Part::Decorator
          def validate
            create_error 'Unknown vehicle_journey_id' unless model_name.vehicle_journey_ids.include?(vehicle_journey_id)

            if stop_area_id
              create_error 'Unknown stop_area_id' unless model_name.stop_area_ids.include?(stop_area_id)
            else
              create_error 'No stop_area_id'
            end
          end
        end
      end

      def errors
        @errors ||= []
      end

      # Represents an Error on a given resource
      class Error
        def initialize(resource, message)
          @resource = resource
          @message = message
        end

        attr_reader :resource, :message

        def to_s
          "#{resource.model_name}/#{resource.resource_name} ##{resource.id}: #{message}"
        end
      end
    end
  end
end
