# frozen_string_literal: true

# operator,Id,ModelName,Name,Code
# stop_area,Id,ParentId,ReferentId,ModelName,Name,Codes,LineIds,Attributes,References,CollectedAlways,CollectChildren,CollectGeneralMessages
# line,Id,ModelName,Name,Codes,Attributes,References,CollectGeneralMessages
# vehicle_journey,Id,ModelName,Name,Codes,LineId,OriginName,DestinationName,Attributes,References
# stop_visit,Id,ModelName,Codes,StopAreaId,VehicleJourneyId,PassageOrder,Schedules,Attributes,References

module Ara
  module File
    class Resource
      class << self
        attr_accessor :attrs

        def attributes
          @attributes ||= []
        end

        def resource_name
          @resource_name ||= name.gsub('Ara::File::', '').gsub(/([a-z]+)([A-Z])/, '\1_\2').downcase.freeze
        end
        alias csv_name resource_name

        def attribute(attr, opts = {})
          attr_accessor attr

          attributes << Attribute.new(attr, **opts)
        end
      end

      def initialize(attributes = {})
        attributes.each { |k, v| send "#{k}=", v }
      end

      def class_attributes
        self.class.attributes
      end

      def resource_name
        self.class.resource_name
      end
      alias csv_name resource_name

      def csv_attrs
        csv = [csv_name]
        class_attributes.each do |attr|
          csv << attribute_value(attr)
        end
        csv
      end

      def attribute_value(attr)
        v = send(attr.name)
        return v if attr.kind == 'Boolean' && ([true, false].include? v)

        v ||= attr.default
        raise InvalidAttributeError, "#{v} (#{attr.name}) is not a #{attr.kind}" unless valid_type(v, attr.kind)

        v = v.to_json if attr.kind == Hash || attr.kind == Array
        v
      end

      def valid_type(v, kind)
        return [true, false].include? v if kind == 'Boolean'

        v.is_a? kind
      end
    end

    class Attribute
      attr_accessor :name, :kind, :default

      def initialize(name, kind: String, default: '')
        @name = name
        @default = default
        @kind = kind
      end
    end

    class InvalidAttributeError < StandardError; end
  end
end
