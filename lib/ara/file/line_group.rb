# frozen_string_literal: true

module Ara
  module File
    class LineGroup < Ara::File::Resource
      attribute :id
      attribute :model_name
      attribute :name
      attribute :short_name
      attribute :line_ids, kind: Array, default: []
    end
  end
end
