# frozen_string_literal: true

module Ara
  module File
    class StopArea < Ara::File::Resource
      attribute :id
      attribute :parent_id
      attribute :referent_id
      attribute :model_name
      attribute :name
      attribute :codes, kind: Hash, default: {}
      attribute :line_ids, kind: Array, default: []
      attribute :attributes, kind: Hash, default: {}
      attribute :references, kind: Hash, default: {}
      attribute :collected_always, kind: 'Boolean', default: true
      attribute :collect_children, kind: 'Boolean', default: false
      attribute :collect_situations, kind: 'Boolean', default: true
    end
  end
end
