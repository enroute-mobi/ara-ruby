# frozen_string_literal: true

# vehicle_journey,Id,ModelName,Name,Codes,LineId,OriginName,DestinationName,Attributes,References

module Ara
  module File
    class VehicleJourney < Ara::File::Resource
      attribute :id
      attribute :model_name
      attribute :name
      attribute :codes, kind: Hash, default: {}
      attribute :line_id
      attribute :origin_name
      attribute :destination_name
      attribute :attributes, kind: Hash, default: {}
      attribute :references, kind: Hash, default: {}
      attribute :direction_type
    end
  end
end
