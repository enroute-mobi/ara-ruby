# frozen_string_literal: true

module Ara
  module File
    class Line < Ara::File::Resource
      attribute :id
      attribute :model_name
      attribute :name
      attribute :codes, kind: Hash, default: {}
      attribute :attributes, kind: Hash, default: {}
      attribute :references, kind: Hash, default: {}
      attribute :collect_situations, kind: 'Boolean', default: true
      attribute :number
      attribute :referent_id
    end
  end
end
