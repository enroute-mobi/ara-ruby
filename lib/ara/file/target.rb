# frozen_string_literal: true

module Ara
  module File
    # Write a file using Ara import format
    class Target
      def initialize(file)
        @file = file
        @model_names = {}
      end

      def model_name(day)
        @model_names[day] ||= Ara::File::Target::ModelName.new(day)
        yield @model_names[day]
      end

      def close
        CSV.open(@file, 'wb') do |csv|
          @model_names.each_value do |model_name|
            model_name.export csv
          end
        end
      end

      # Regroups resources by Model Name
      class ModelName
        def operators
          @operators ||= []
        end

        def stop_areas
          @stop_areas ||= []
        end

        def stop_area_groups
          @stop_area_groups ||= []
        end

        def lines
          @lines ||= []
        end

        def line_groups
          @line_groups ||= []
        end

        def vehicle_journeys
          @vehicle_journeys ||= []
        end

        def stop_visits
          @stop_visits ||= []
        end

        def initialize(day)
          @model_name = day.strftime('%F')
        end

        attr_reader :model_name

        def add(resource)
          resource.model_name = model_name
          collection(resource) << resource
        end
        alias << add

        def collection(resource)
          send("#{resource.class.csv_name}s")
        end

        def export(csv)
          [operators,
           stop_areas,
           stop_area_groups,
           lines,
           line_groups,
           vehicle_journeys,
           stop_visits].each do |collection|
            collection.each do |resource|
              csv << resource.csv_attrs
            end
          end
        end
      end
    end
  end
end
