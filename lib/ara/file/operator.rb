# frozen_string_literal: true

module Ara
  module File
    class Operator < Ara::File::Resource
      attribute :id
      attribute :model_name
      attribute :name
      attribute :codes, kind: Hash, default: {}
    end
  end
end
