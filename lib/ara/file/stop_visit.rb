# frozen_string_literal: true

module Ara
  module File
    class StopVisit < Ara::File::Resource
      attribute :id
      attribute :model_name
      attribute :codes, kind: Hash, default: {}
      attribute :stop_area_id
      attribute :vehicle_journey_id
      attribute :passage_order
      attribute :schedules, kind: Array, default: []
      attribute :attributes, kind: Hash, default: {}
      attribute :references, kind: Hash, default: {}
    end
  end
end
