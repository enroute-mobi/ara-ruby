# frozen_string_literal: true

module Ara
  module File
    # Read a file using Ara import format
    class Source
      attr_reader :file_path

      def initialize(file_path)
        @file_path = file_path
      end

      def stop_areas
        ResourceReader.new(file_path, Ara::File::StopArea).resources
      end

      def stop_area_groups
        ResourceReader.new(file_path, Ara::File::StopAreaGroup).resources
      end

      def lines
        ResourceReader.new(file_path, Ara::File::Line).resources
      end

      def line_groups
        ResourceReader.new(file_path, Ara::File::LineGroup).resources
      end

      def operators
        ResourceReader.new(file_path, Ara::File::Operator).resources
      end

      def vehicle_journeys
        ResourceReader.new(file_path, Ara::File::VehicleJourney).resources
      end

      def stop_visits
        ResourceReader.new(file_path, Ara::File::StopVisit).resources
      end

      class ResourceReader
        attr_reader :file_path, :resource_class

        def initialize(file_path, resource_class)
          @file_path = file_path
          @resource_class = resource_class
        end

        def each(&block)
          CSV.foreach(file_path, 'r') do |row|
            next unless row[0] == resource_class.csv_name

            block.call create(row)
          end
        end

        def csv_attribute_names
          @csv_attribute_names ||= resource_class.attributes.map(&:name)
        end

        def self.json_parser
          @json_parser = proc { |csv_value| JSON.parse(csv_value) }
        end

        def csv_transformers
          @csv_transformers = resource_class.attributes.map do |attribute|
            self.class.json_parser if [Hash, Array, 'Boolean'].include? attribute.kind
          end
        end

        def create(row)
          raw_values = row[1..]

          transformers_and_values = [csv_transformers, raw_values].transpose
          transformed_values = transformers_and_values.map do |transformer, raw_value|
            raw_value = nil if raw_value == ''
            transformer ? transformer.call(raw_value) : raw_value
          end
          attributes = [csv_attribute_names, transformed_values].transpose
          resource_class.new attributes
        end

        def resources
          enum_for :each
        end
      end
    end
  end
end
