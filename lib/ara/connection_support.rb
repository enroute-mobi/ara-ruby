# frozen_string_literal: true

module Ara
  # Manage Connection provider
  module ConnectionSupport
    attr_reader :connection_provider

    def connected_with(connection_provider)
      @connection_provider = connection_provider
      self
    end
  end
end
