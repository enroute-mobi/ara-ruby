# frozen_string_literal: true

module Ara
  # Manage Ara Referential Situation
  class Situation < Resource
    class InfoLink
      attr_accessor :uri, :label, :image_ref

      def initialize(args)
        @uri = args['uri'] || args['Uri']
        @label = args['label'] || args['Label']
        @image_ref = args['image_ref'] || args['ImageRef']
      end

      def to_h
        {
          'Uri': uri,
          'Label': label,
          'ImageRef': image_ref
        }
      end
    end

    class TimeRange
      attr_accessor :start_time, :end_time

      def initialize(args)
        @start_time = Time.parse(args['StartTime']) unless args['StartTime'].nil?
        return if args['EndTime'].nil?

        @end_time = Time.parse(args['EndTime'])
      end

      def to_h
        hash = {}
        hash['StartTime'] = (start_time.to_datetime.to_s unless start_time.nil?)
        hash['EndTime'] = (end_time.to_datetime.to_s unless end_time.nil?)

        hash
      end
    end

    class TranslatedString
      class Translation
        attr_reader :language, :text

        def initialize(language:, text:)
          @language = language
          @text = text
        end

        def to_h
          {
            @language => @text
          }
        end
      end

      attr_accessor :default_value, :translations

      def initialize(args)
        @default_value = args['default_value'] || args['DefaultValue']

        return unless args['Translations'] || args['translations']

        @translations = (args['Translations'] || args['translations']).map do |k, v|
          Translation.new(language: k, text: v)
        end
      end

      def translation(locale)
        translations&.find { |t| t.language.downcase == locale }
      end

      def default_value?
        !default_value.nil?
      end

      def to_h
        {
          'DefaultValue': default_value,
          'Translations': translations&.map(&:to_h)&.reduce({}, :merge)
        }
      end
    end

    attr_accessor :affects,
                  :alert_cause,
                  :codes,
                  :consequences,
                  :format,
                  :id,
                  :internal_tags,
                  :origin,
                  :participant_ref,
                  :producer_ref,
                  :progress,
                  :publication_windows,
                  :publish_to_web_actions,
                  :publish_to_mobile_actions,
                  :publish_to_display_actions,
                  :reality,
                  :recorded_at,
                  :report_type,
                  :severity,
                  :version,
                  :versioned_at

    attr_reader :summary, :description, :info_links, :validity_periods

    def summary=(values)
      @summary = TranslatedString.new(values)
    end

    def description=(values)
      @description = TranslatedString.new(values)
    end

    def info_links=(values)
      return if values.nil?

      @info_links = values.map { |link| InfoLink.new(link) }
    end

    def validity_periods=(values)
      return if values.nil?

      @validity_periods = values
                          .select { |period| period['Startime'].nil? && period['Endtime'].nil? }
                          .map { |period| TimeRange.new(period) }
    end

    attr_writer :code_space, :situation_number, :keywords

    def code_space
      return @code_space if codes.nil?

      codes.reject { |c| c['_default'] }.keys[0]
    end

    def situation_number
      return @situation_number if codes.nil?

      codes.reject { |c| c['_default'] }.values[0]
    end

    def keywords
      @keywords&.join(', ')
    end

    def api_attributes
      api_attributes = {
        alert_cause: alert_cause,
        codes: codes,
        code_space: code_space,
        consequences: consequences,
        format: format,
        id: id,
        internal_tags: internal_tags,
        origin: origin,
        participant_ref: participant_ref,
        producer_ref: producer_ref,
        progress: progress,
        publication_windows: publication_windows,
        reality: reality,
        recorded_at: recorded_at,
        report_type: report_type,
        severity: severity,
        situation_number: situation_number,
        version: version,
        versioned_at: versioned_at
      }.compact!

      api_attributes[:affects] = affects.to_a.map(&:to_h)
      api_attributes[:summary] = summary.to_h if summary
      api_attributes[:description] = description.to_h if description
      api_attributes[:validity_periods] = validity_periods.to_a.map(&:to_h)
      api_attributes[:keywords] = keywords.split(',').map(&:strip) if keywords
      api_attributes[:info_links] = info_links.to_a.map(&:to_h)

      api_attributes
    end
  end
end
