# frozen_string_literal: true

module Ara
  # Manage Ara Server
  class Server
    attr_reader :host, :token

    def initialize(host:, token:)
      @host = host
      @token = token
    end

    def connection
      Connection.new(host).tap do |connection|
        connection.token = token
      end
    end

    def referentials
      @referentials ||= Ara::Referentials.new.connected_with(self)
    end
  end
end
