# frozen_string_literal: true

module Ara
  # Manage resource errors
  class Error
    attr_accessor :attribute, :type, :message

    def initialize(attribute, type, message)
      @attribute = attribute
      @type = type
      @message = message
    end
  end

  # Parse JSON content returned by the Ara API
  #
  #   {"Slug"=>["Invalid format: only lowercase alphanumeric characters and _"]}
  #    =>
  #    [
  #      Error.new("slug", "invalid_format" type, message: "")
  #    ]
  #
  # https://github.com/rafaelfranca/omg-rails/blob/master/activemodel/lib/active_model/errors.rb#L406
  class ErrorParser
    def initialize(raw_value)
      @raw_value = raw_value
    end

    def json_value
      @raw_value
    end

    def errors
      @errors ||= []
    end

    def parse
      json_value[:errors].each do |raw_attribute, messages|
        attribute = underscore(raw_attribute)
        messages.each do |message|
          errors << Error.new(attribute, parse_type(message), message)
        end
      end

      errors
    rescue JSON::ParserError
      errors
    end

    def underscore(attribute_name)
      attribute_name.to_s
                    .gsub(/([A-Z]+)([A-Z][a-z])/, '\1_\2')
                    .gsub(/([a-z\d])([A-Z])/, '\1_\2').downcase
    end

    def parse_type(message) # rubocop:disable Metrics/MethodLength
      case message
      when /Invalid format/, /Invalid request/
        :invalid
      when /Can't be empty/
        :blank
      when /Can't be zero/
        :zero
      when /Is already in use/
        :uniqueness
      else
        :invalid
      end
    end
  end
end
