# frozen_string_literal: true

module Ara
  # Manage Ara Referential lines collection
  class Lines < Collection; end
end
