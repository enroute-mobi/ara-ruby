# frozen_string_literal: true

module Ara
  # Manage Ara Referential Partner
  class Partner < Resource
    attr_accessor :slug, :name, :partner_status, :connector_types, :settings

    def status
      partner_status && partner_status['OperationnalStatus']
    end

    def api_attributes
      {
        slug: slug,
        name: name,
        connector_types: connector_types,
        settings: settings
      }.compact
    end
  end
end
