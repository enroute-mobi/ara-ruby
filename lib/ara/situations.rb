# frozen_string_literal: true

module Ara
  # Manage Ara Referential situations collection
  class Situations < Collection
    class Affects; end
  end
end
